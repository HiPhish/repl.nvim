" Author: Alejandro "HiPhish" Sanchez
" License:  The MIT License (MIT) {{{
"    Copyright (c) 2017-2021 HiPhish
" 
"    Permission is hereby granted, free of charge, to any person obtaining a
"    copy of this software and associated documentation files (the
"    "Software"), to deal in the Software without restriction, including
"    without limitation the rights to use, copy, modify, merge, publish,
"    distribute, sublicense, and/or sell copies of the Software, and to permit
"    persons to whom the Software is furnished to do so, subject to the
"    following conditions:
" 
"    The above copyright notice and this permission notice shall be included
"    in all copies or substantial portions of the Software.
" 
"    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
"    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
"    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
"    NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
"    DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
"    OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
"    USE OR OTHER DEALINGS IN THE SOFTWARE.
" }}}

if !has('nvim')
  finish
endif


" ----------------------------------------------------------------------------
" The default settings
" ----------------------------------------------------------------------------
" bin     : Which REPL binary to execute
" args    : Arguments to pass to every execution, come before user arguments
" syntax  : Syntax highlighting to use for the REPL buffer
" title   : Value of b:term_title
" preproc : Funcref to a text preprocessor before sending it to the REPL
" ----------------------------------------------------------------------------
let s:repls = {
	\ 'elixir': {
		\ 'bin': 'iex',
		\ 'args': { -> filereadable('mix.exs') ? ['-S', 'mix'] : []},
		\ 'title': 'IEx'
	\},
	\ 'erlang': {
		\ 'bin': 'erl',
		\ 'title': 'Erlang shell'
	\},
	\ 'fennel': {
		\ 'bin': 'fennel',
		\ 'args': [],
		\ 'title': 'Fennel'
	\},
	\ 'guile': {
		\ 'bin': 'guile',
		\ 'args': ['-L', '.'],
		\ 'syntax': 'scheme',
		\ 'title': 'Guile REPL',
	\ },
	\ 'java': {
		\ 'title': 'JShell',
		\ 'bin': 'jshell',
		\ 'syntax' : 'java',
	\ },
	\ 'javascript': {
		\ 'title': 'Node.js',
		\ 'bin': 'node',
		\ 'args': ['--interactive'],
	\ },
	\ 'lisp': {
		\ 'bin': 'sbcl',
		\ 'syntax': 'lisp',
		\ 'title': 'SBLC REPL',
	\ },
	\ 'lua': {
		\ 'bin': 'lua',
		\ 'title': 'Lua',
	\ },
	\ 'nim': {
		\ 'title': 'INim',
		\ 'bin': executable('inim') ? 'inim' : expand('~/.nimble/bin/inim'),
		\ 'syntax': 'nim',
	\ },
	\ 'python': {
		\ 'bin': 'python',
		\ 'title': 'Python REPL',
		\ 'preproc': {txt -> join(filter(split(txt, '\n'), {idx, val -> !empty(val)}), "\n")}
	\ },
	\ 'r7rs-small': {
		\ 'bin': 'chibi-scheme',
		\ 'syntax': 'scheme',
		\ 'title': 'Chibi Scheme',
	\ },
	\ 'racket': {
		\ 'bin': 'racket',
		\ 'syntax': 'racket',
		\ 'title': 'Racket',
	\ },
	\ 'scala': {
		\ 'bin': executable('scala') ? 'scala' : 'sbt',
		\ 'args': executable('scala') ? [] : ['console'],
		\ 'title': 'Scala',
	\},
	\ 'scheme.mit': {
		\ 'bin': 'mit-scheme',
		\ 'title': 'MIT/GNU Scheme',
		\ 'syntax': 'scheme',
	\ },
	\ 'sh': {
		\ 'bin': 'sh',
		\ 'title': 'Bourne Shell',
	\ },
\ }

" ----------------------------------------------------------------------------
let s:repls['python3'] = copy(s:repls['python'])
let s:repls['python3']['bin'] = 'python3'

let s:repls['r7rs'] = copy(s:repls['r7rs-small'])
let s:repls['scheme'] = copy(s:repls['r7rs-small'])
" ----------------------------------------------------------------------------


" Build a dictionary to hold global setting if there is none
if !has_key(g:, 'repl')
	let g:repl = {}
endif

" Assign the default options, respect user settings
for [s:type, s:repl] in items(s:repls)
	call repl#define_repl(s:type, s:repl, 'keep')
endfor
